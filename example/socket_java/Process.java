import java.io.IOException;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;

public class Process extends Thread 
{
	private Socket socket;
	
	public Process(Socket socket)
	{
		this.socket = socket;
	}
	
	@Override
	public void run()
	{
		try
		{
			//读取req json长度
			byte[] lenBytes = new byte[12];	
			socket.getInputStream().read(lenBytes);
			String lenStr = new String(lenBytes).trim();
			int len = Integer.parseInt(lenStr);
//			System.out.println("req json length: " + len);
			
			//读取req json
			byte[] data = new byte[len];
			socket.getInputStream().read(data); //这里未考虑tcp的拆包
			String jsonStr = new String(data, "utf-8");
			
			//解析req json
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> map = mapper.readValue(jsonStr, Map.class);
			String methodStr = (String) ((Map) map.get("head")).get("method");
			String[] methods = methodStr.split("::");
			String clazzName = methods[0];	//类名 aaa.BBB
			String methodName = methods[1];	//方法名 hello
//			System.out.println("request class:" + clazzName + ", method:" + methodName);
			
			//java反射调用aaa.BBB类中hello方法
			Class<?> clazz = Class.forName(clazzName);
			Method method = clazz.getMethod(methodName, Object.class);
			String rspJson = (String) method.invoke(clazz.newInstance(), map.get("data"));
			
			//返回 rep json
			byte[] rspBytes = rspJson.getBytes();
			lenStr = String.format("%010d\r\n", rspBytes.length); 
			socket.getOutputStream().write(lenStr.getBytes());		//socket返回 长度行
			socket.getOutputStream().write(rspJson.getBytes());		//socket返回 rsp json
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try { socket.close();} catch (IOException e) {}
		}
	}
}
