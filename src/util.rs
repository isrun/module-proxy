
// This file is part of Module Proxy.

// Module Proxy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Module Proxy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Module Proxy.  If not, see <https://www.gnu.org/licenses/>.


//         Copyright (C) 2021 - 2030  关中麦客  
//         All rights reserved
//
//         util.rs
//         通用工具模块
//
//         created by 关中麦客 1036038462@qq.com

use std::time::{SystemTime, UNIX_EPOCH};
use std::{thread,time};
use rand::Rng;

//毫秒时间戳
pub fn timestamp() -> u64
{
    let start = SystemTime::now();
    let since_the_epoch = start
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");
    let ms = since_the_epoch.as_secs()  * 1000u64 
            + (since_the_epoch.subsec_nanos() as f64 / 1_000_000.0) as u64;
    ms
}

//秒时间戳
pub fn sec_timestamp() -> u32
{
    let mtime = timestamp();
    (mtime / 1000) as u32
}

///延时exit
pub fn _exit()
{
    let ten_millis = time::Duration::from_millis(2000);
    thread::sleep(ten_millis);  //休眠2秒
    std::process::exit(1);
}

///在半开放的 [n, m) 范围内（不包括 m）生成一个随机值
pub fn rand(n: u32, m:u32) -> u32
{
    let mut rng = rand::thread_rng();
    rng.gen_range(n..m)
}

// ----------------- test -------------------
#[test]
fn test_rand()
{
    for i in (0..10000000).rev()
    {
        let n = i + 10;
        let m = i + 10000;
        let r = rand(n, m);
        assert!(n <= r && r < m);
    }
}